{ stdenv, lib, pkgsBuildTarget, targetPackages, fetchurl, gnutar

# configuration
, version
, src
, haskell
, bootGhc
  # Options to pass to ./hadrian-nixpkgs.nix or null, in
  # which case ./hadrian-bootstrap.nix is used
, hadrianBootstrapOpts ? null

  # Default to building in the same flavour used to produced upstream bindists
, buildFlavour ? (if builtins.compareVersions version "9.4" < 0 then "perf" else "release")
, hashUnitIds  ? builtins.compareVersions version "9.8" >= 0
, enableIntegerSimple ? false
, patches ? []

, useLLVM ? !stdenv.targetPlatform.isx86
, # LLVM is conceptually a run-time-only depedendency, but for
  # non-x86, we need LLVM to bootstrap later stages, so it becomes a
  # build-time dependency too.
  buildLlvmPackages, llvmPackages

, # Whether to disable the large address space allocator
  # necessary fix for iOS: https://www.reddit.com/r/haskell/comments/4ttdz1/building_an_osxi386_to_iosarm64_cross_compiler/d5qvd67/
  disableLargeAddressSpace ? stdenv.targetPlatform.isDarwin && stdenv.targetPlatform.isAarch64

, # If enabled, use -fPIC when compiling static libs.
  enableRelocatedStaticLibs ? stdenv.targetPlatform != stdenv.hostPlatform

, # Whether to build terminfo.
  enableTerminfo ? !stdenv.targetPlatform.isWindows

  # Whether to enable the RTS's NUMA support
, enableNuma ? stdenv.targetPlatform.isLinux

# build dependencies
, automake, autoconf
, python3, sphinx
, glibcLocales
, libffi
, libiconv ? null
, ncurses
, happy
, alex
, hscolour
, gmp
, numactl
}:

let
  hadrianArgs = "--docs=no-sphinx-pdfs"
              + (if hashUnitIds
                  then " --hash-unit-ids"
                  else "")
              + (if sphinx == null then " --docs=no-sphinx" else "");


  hadrian =
    if hadrianBootstrapOpts == null
    then import ./hadrian-bootstrap.nix {
      inherit lib stdenv fetchurl gnutar src bootGhc python3 version;
    }
    else import ./hadrian-nixpkgs.nix {
      inherit lib haskell src alex happy;
      inherit (hadrianBootstrapOpts) shakeVersion haskellPkgs;
    };
in

let
  inherit (stdenv) buildPlatform hostPlatform targetPlatform;

  # Splicer will pull out correct variations
  libDeps = platform:
       lib.optional enableTerminfo ncurses
    ++ [libffi]
    ++ lib.optional (!enableIntegerSimple) gmp
    ++ lib.optional (platform.libc != "glibc" && !targetPlatform.isWindows) libiconv;

  toolsForTarget = [
    pkgsBuildTarget.targetPackages.stdenv.cc
  ] ++ lib.optional useLLVM buildLlvmPackages.llvm;

  targetCC = builtins.head toolsForTarget;

  # ld.gold is disabled for musl libc due to https://sourceware.org/bugzilla/show_bug.cgi?id=23856
  # see #84670 and #49071 for more background.
  useLdGold = targetPlatform.isLinux && !(targetPlatform.useLLVM or false) && !targetPlatform.isMusl;

  ghc = stdenv.mkDerivation {
    name = "ghc-${version}";
    inherit version src patches;
    nativeBuildInputs = [
      automake autoconf hadrian python3 sphinx hscolour
      bootGhc
    ] ++ lib.optional enableNuma numactl;
    buildInputs = [
      gmp
    ];

    # Make sure we never relax`$PATH` and hooks support for compatibility.
    strictDeps = true;

    # Don’t add -liconv to LDFLAGS automatically so that GHC will add it itself.
    dontAddExtraLibs = true;

    propagatedBuildInputs = [
      targetPackages.stdenv.cc
    ] ++ lib.optional enableNuma numactl;
    depsTargetTarget = map lib.getDev (libDeps targetPlatform);
    depsTargetTargetPropagated = map (lib.getOutput "out") (libDeps targetPlatform);

    preConfigure = ''
      for env in $(env | grep '^TARGET_' | sed -E 's|\+?=.*||'); do
        export "''${env#TARGET_}=''${!env}"
      done
      # GHC is a bit confused on its cross terminology, as these would normally be
      # the *host* tools.
      export CC="${targetCC}/bin/${targetCC.targetPrefix}cc"
      export CXX="${targetCC}/bin/${targetCC.targetPrefix}c++"
      # Use gold to work around https://sourceware.org/bugzilla/show_bug.cgi?id=16177
      export LD="${targetCC.bintools}/bin/${targetCC.bintools.targetPrefix}ld${lib.optionalString useLdGold ".gold"}"
      export AS="${targetCC.bintools.bintools}/bin/${targetCC.bintools.targetPrefix}as"
      export AR="${targetCC.bintools.bintools}/bin/${targetCC.bintools.targetPrefix}ar"
      export NM="${targetCC.bintools.bintools}/bin/${targetCC.bintools.targetPrefix}nm"
      export RANLIB="${targetCC.bintools.bintools}/bin/${targetCC.bintools.targetPrefix}ranlib"
      export READELF="${targetCC.bintools.bintools}/bin/${targetCC.bintools.targetPrefix}readelf"
      export STRIP="${targetCC.bintools.bintools}/bin/${targetCC.bintools.targetPrefix}strip"
      export ALEX="${alex}/bin/alex"
      export HAPPY="${happy}/bin/happy"

      if [[ -f ./boot ]]; then
        python3 ./boot
      fi
    '';

    configureFlags = [
      "--datadir=$doc/share/doc/ghc"
      "--with-curses-includes=${ncurses.dev}/include"
      "--with-curses-libraries=${ncurses.out}/lib"
    ] ++ lib.optionals enableNuma [
      "--enable-numa"
      "--with-libnuma-includes=${targetPackages.numactl.dev}/include"
      "--with-libnuma-libraries=${targetPackages.numactl.out}/lib"
    ] ++ lib.optionals (libffi != null) [
      "--with-system-libffi"
      "--with-ffi-includes=${targetPackages.libffi.dev}/include"
      "--with-ffi-libraries=${targetPackages.libffi.out}/lib"
    ] ++ lib.optionals (targetPlatform == hostPlatform && !enableIntegerSimple) [
      "--with-gmp-includes=${targetPackages.gmp.dev}/include"
      "--with-gmp-libraries=${targetPackages.gmp.out}/lib"
    ] ++ lib.optionals (targetPlatform == hostPlatform && hostPlatform.libc != "glibc" && !targetPlatform.isWindows) [
      "--with-iconv-includes=${libiconv}/include"
      "--with-iconv-libraries=${libiconv}/lib"
    ] ++ lib.optionals (targetPlatform != hostPlatform) [
      "--enable-bootstrap-with-devel-snapshot"
    ] ++ lib.optionals useLdGold [
      "CFLAGS=-fuse-ld=gold"
      "CONF_GCC_LINKER_OPTS_STAGE1=-fuse-ld=gold"
      "CONF_GCC_LINKER_OPTS_STAGE2=-fuse-ld=gold"
    ] ++ lib.optionals (disableLargeAddressSpace) [
      "--disable-large-address-space"
    ];

    # Hadrian attempts to capture builders' output; ensure that it uses a
    # Unicode locale.
    LOCALE_ARCHIVE = "${glibcLocales}/lib/locale/locale-archive";
    LANG = "C.UTF-8";
    BUILD_FLAVOUR = "${buildFlavour}";

    buildPhase = ''
      hadrian -j$NIX_BUILD_CORES --flavour="$BUILD_FLAVOUR" ${hadrianArgs}
    '';

    installPhase = ''
      hadrian -j$NIX_BUILD_CORES --flavour="$BUILD_FLAVOUR" ${hadrianArgs} install --prefix="$out"
    '';

    passthru = {
      haskellcompilerName = "ghc-${version}";
    };

    meta = {
      homepage = "http://haskell.org/ghc";
      description = "The Glasgow Haskell Compiler";
      maintainers = with lib.maintainers; [ marcweber andres peti ];
      ghcBuildSystem = "hadrian";
      timeout = 24 * 3600;
    };
  };

in { ghc = ghc;
     hadrian = hadrian; }
