{ nixImageTag }:

let
  sources = import ./nix/sources.nix;
  baseNixpkgs = import sources.nixpkgs {};
in
with baseNixpkgs;

let
  drvs = import ./.;

  toJob = drvName:
    ''
      build-${drvName}:
        extends: .build
        variables:
          DRV_NAME: "${drvName}"
    '';

  ci-jobs =
    ''
      .build:
        image: nixos/nix:${nixImageTag}
        stage: build
        variables:
          CACHIX_AUTH_TOKEN: unset
        tags:
          - x86_64-linux
        before_script:
          - |
            cat >/etc/nix/nix.conf <<EOF
            sandbox = false
            build-users-group =
            trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= loony-tools:pr9m4BkM/5/eSTZlkQyRt57Jz7OMBxNSUiMC4FkcNfk=
            substituters = https://cache.nixos.org https://cache.zw3rk.com
            experimental-features = nix-command flakes
            EOF

        script:
          - ./ci/build.sh
    '' + lib.concatMapStringsSep "\n" toJob (lib.attrNames drvs.ciDrvs);
in { inherit ci-jobs; }
